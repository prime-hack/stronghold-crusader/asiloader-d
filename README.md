[![pipeline status](https://gitlab.com/prime-hack/stronghold-crusader/asiloader-d/badges/master/pipeline.svg)](https://gitlab.com/prime-hack/stronghold-crusader/asiloader-d/-/commits/master)

## About AsiLoader

AsiLoader is loader of outside libraries with `.asi` extension

## Install

Move file [SHFOLDER.dll](https://gitlab.com/prime-hack/stronghold-crusader/asiloader-d/-/pipelines) to game folder

## Build

```bash
dub build -brelease
```

#### Cross-compilation

For cross-compilation your need use **[LDC2](https://github.com/ldc-developers/ldc/releases)**
```bash
dub build -brelease --compiler=ldc2 --arch=i686-pc-windows-msvc
```

## ASI example

This is example of library for AsiLoader

```c
#include <windows.h>
BOOL APIENTRY DllMain( HMODULE h, DWORD dwReasonForCall, LPVOID v ) {
	if (dwReasonForCall == 1)
		MessageBoxA(0,0,0,0);
	return TRUE;
}
```

After compile your get dll-library. Change extension of library to `.asi` and place it to game folder. After game run you view empty message box or black screen (`alt-tab` for view message box)