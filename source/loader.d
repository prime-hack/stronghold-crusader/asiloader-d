import core.sys.windows.windows, core.sys.windows.dll, core.runtime;
import std.file : dirEntries, getcwd, SpanMode;
import std.string : toStringz;
import shfolder;

extern(Windows) BOOL DllMain(HINSTANCE hInstance, ULONG ulReason, LPVOID pvReserved) {
    switch (ulReason) {
        case DLL_PROCESS_ATTACH:
            Runtime.initialize();
            init();
            dll_process_attach(hInstance, true);
            break;
        case DLL_PROCESS_DETACH:
            Runtime.terminate();
            dll_process_detach(hInstance, true);
            break;
        case DLL_THREAD_ATTACH:
            dll_thread_attach(true, true);
            break;
        case DLL_THREAD_DETACH:
            dll_thread_detach(true, true);
            break;
        
        default:
            break;
    }
    return true;
}

void init() {
    InitLibProxy();
	foreach (string plugin; dirEntries(getcwd, "*.asi", SpanMode.shallow))
		plugin.toStringz.LoadLibraryA;

}

